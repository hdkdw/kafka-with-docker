# Apache Kafka menggunakan Docker
Implementasi apache kafka terdiri dari 4 container, masing-masing menangani 1 service :
- Zookeeper
- Kafka broker-0 (port yang diakses dari localhost : 19092)
- Kafka broker-1 (port yang diakses dari localhost : 29092)
- Kafka broker-2 (port yang diakses dari localhost : 39092)  
  
semua service tersebut menggunakan baseimage Centos 8.4 dan Apache Kafka versi kafka_2.12-2.8.1

## Running
Untuk menjalankan, gunakan command sbb di directory dimana docker-compose.yml disimpan
```
docker-compose up --build
```  
pastikan semua container sudah up & running

